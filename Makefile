﻿CC = gcc
CFLAGS = -g3 -Wall -o0 -std=gnu99

CXX = g++
CXXFLAGS = -g3 -Wall -o0 -std=gnu++11 -D__TC_FN__=\"$(notdir $<)\"

LDFLAGS = 

OBJ =

TARGET = main.exe
RUN_ARGS = 
OBJS = 
HEADERS = 

TEST_TARGET = main_test.exe
TEST_OBJ = 

RM = rm -f

export OBJ HEADERS TEST_OBJ

-include subdir.mk

run : all
	@echo StartRunning: $(TARGET) with arg: $(RUN_ARGS)
	@./$(TARGET) $(RUN_ARGS)

all : $(TARGET) Makefile

trun : test
	@echo StartRunning Test: $(TEST_TARGET) with arg: $(RUN_ARGS)
	@./$(TEST_TARGET) $(RUN_ARGS)

test : $(TEST_TARGET) Makefile

%.o : %.cpp
	@echo Building File: $<
	@$(CXX) $(CXXFLAGS) $(HEADERS) -c -o $@ $<

%.o : %.c
	@echo Building File: $<
	@$(CC) $(CFLAGS) $(HEADERS) -c -o $@ $<

$(TARGET) : $(OBJS)
	@echo StartBuild: $(TARGET)
	@$(CXX) $(LDFLAGS) -o $(TARGET) $(OBJS)

$(TEST_TARGET) : $(TEST_OBJ) linker.ld
	@echo StartBuild TestTarget: $(TEST_TARGET)
	@$(CXX) $(LDFLAGS) -Wl,-Map=main_test.map -T"linker.ld" -o $(TEST_TARGET) $(TEST_OBJ)

clean:
	@$(RM) $(TARGET) main.o $(TEST_TARGET) $(TEST_OBJ) main_test.map

.PHONY : all clean run


