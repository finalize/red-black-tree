/*
@author: ZZH
@date: 2021-11-05
@time: 14:19:34
@info: 基于双向链表的多内存池管理
*/
#pragma once
#include <stdlib.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define false 0
#define true 1
#define nullptr NULL

#define __init __attribute__((constructor))

typedef struct __MemBlock
{
    struct __MemBlock* next, * prev;
    uint32_t size;
    uint8_t isFree;
    uint32_t mem[0];
} MemBlock_t, * pMemBlock_t;

typedef struct
{
    MemBlock_t* const memStart;
    const void* const memEnd;
    uint32_t availableSize;
} MemPool_t, * pMemPool_t;

#define MemEnd(mem) (mem + ARRAY_LENGTH(mem))
#define ArrayMem(arr) {(pMemBlock_t)arr, MemEnd(arr)}

void __init ZMemInit();
void* ZMalloc(const uint32_t pool, size_t size);
void ZFree(void* pMem);

#ifdef __cplusplus
}
#endif
