/*
@file: RB-Tree-OP.h
@auther: ZZH
@info:定义红黑树的节点操作，包括取父节点，取兄弟节点，判定节点为空等
*/
#pragma once

//求结构体内某成员的偏移量
#ifndef offset_of
#define offset_of(type, member) ((size_t)&((type*)0)->member)
#endif

//已知结构体内某成员的地址和结构体类型,求结构体地址
#ifndef container_of
#define container_of(p, type, member) ((type*)((size_t)p - offset_of(type, member)))
#endif

//交换AB的变量值
#define SWAP(a, b) {typeof(a) __swap_var__ = a; a = b; b = __swap_var__;}

// #define RBT_IS_NIL(p) ((p->key | p->value | p->left | p->right | p) == NULL)
#define RBT_IS_NIL(p) ((p) == NULL)

#define RBT_PARENT(p) (p->parent)
#define RBT_BROTHER_L(p) (RBT_PARENT(p)->left)
#define RBT_BROTHER_R(p) (RBT_PARENT(p)->right)
#define RBT_GET_BROTHER(p) (p == RBT_BROTHER_L(p) ? RBT_BROTHER_R(p) : RBT_BROTHER_L(p))
#define RBT_GRANDPARENT(p) (RBT_PARENT(RBT_PARENT(p)))
#define RBT_UNCLE_L(p) (RBT_GRANDPARENT(p)->left)
#define RBT_UNCLE_R(p) (RBT_GRANDPARENT(p)->right)
#define RBT_GET_UNCLE(p) (RBT_PARENT(p) == RBT_UNCLE_L(p) ? RBT_UNCLE_R(p) : RBT_UNCLE_L(p))
#define RBT_GET_COLOR(p) (RBT_IS_NIL(p) ? RBT_Black : p->color)//空指针NIL节点,NIL算黑节点
