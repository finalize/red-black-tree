#include "RB-Tree-Base.h"
#include "RB-Tree-OP.h"
#include "RB-Tree-Visual.h"
#include "RB-Tree-IntString.h"
#include "stdio.h"

static void padding(char ch, int n)
{
    for (int i = 0; i < n; i++)
        putchar(ch);
}

static void print_node(pRBTreeNode_t root, int level) //中序遍历
{
    if (root == NULL)
    {
        padding('\t', level);
        puts("NIL");
    }
    else
    {
        print_node(root->right, level + 1);
        padding('\t', level);

        const char *fmt = (root->color == RBT_Black) ? "(%d)\n" : "%d\n";

        printf(fmt, container_of(root, RB_treeNode_IntString_t, node)->key);

        print_node(root->left, level + 1);
    }
}

void RBT_Visual(pRBTree_t tree)
{
    print_node(tree->root, 0);
}