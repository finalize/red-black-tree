HEADERS += -I"./RB-Tree/testCases"

files := $(shell ls ./RB-Tree/testCases/tc_*.cpp)
files := $(files:%.cpp=%.o)

TEST_OBJ += $(files)
