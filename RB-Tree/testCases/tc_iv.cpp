#include "testFrame.h"
#include "RB-Tree-IntpVoid.h"
#include <exception>
#include <stdexcept>
#include "MemMana.h"

using namespace std;

extern MemPool_t __MemPools__[];

static bool tciv(void)
{
    //测试点数和结果
    const size_t testPoints = 10;
    bool res = true;

    //创建树
    pRBTree_t tree = RBT_Create_IntpVoid();
    if (tree == nullptr)
        throw runtime_error("红黑树创建失败");

    //插入键值对
    for (size_t i = 0;i < testPoints;i++)
        RBT_Insert_IntpVoid(tree, i, (void*) (i + 1));

    //检查每一个键值对对应关系是否正确
    for (size_t i = 0;i < testPoints;i++)
        res &= (i + 1 == (size_t) RBT_Search_IntpVoid(tree, i, RBT_Equal, nullptr));

    //这里可有可无,因为下方会清理整棵树
    // for (size_t i = 0;i < testPoints;i++)
    //     RBT_DeleteNodeByKey_IntpVoid(tree, i);

    //清理整棵树并检查内存是否完全释放
    RBT_Delete_IntpVoid(tree);
    res &= __MemPools__[0].availableSize == 409600;

    //返回测试结果给测试框架
    return res;
}
ExportTestCase(tciv, 0);

