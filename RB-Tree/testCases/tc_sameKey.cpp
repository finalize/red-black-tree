#include "testFrame.h"
#include "RB-Tree-IntpVoid.h"
#include <exception>
#include <stdexcept>
#include "MemMana.h"

using namespace std;

extern MemPool_t __MemPools__[];

static bool sameKey()
{
    const size_t testPoints = 10;

    bool res = true;

    //创建树
    pRBTree_t tree = RBT_Create_IntpVoid();
    if (tree == nullptr)
        throw runtime_error("红黑树创建失败");

    //连续十次插入同一个键
    for (size_t i = 0;i < testPoints;i++)
        RBT_Insert_IntpVoid(tree, 1, (void*) (i + 1));

    //检查键是否为最后一次插入的值
    res &= RBT_Search_IntpVoid(tree, 1, RBT_Equal, nullptr) == (void*) testPoints;

    //删除整棵树,然后检查内存是否完全释放
    RBT_Delete_IntpVoid(tree);
    res &= __MemPools__[0].availableSize == 409600;

    //返回测试结果给测试框架
    return res;
}
ExportTestCase(sameKey, 1);
