#pragma once
#include "operations.h"

#ifndef __TC_FN__
#define __TC_FN__ ""
#endif

typedef struct
{
    void (*cb)(void);
    const char* name;
}InitInfo_t, * pInitInfo_t;

typedef struct
{
    bool (*cb)(void);
    const char* name;
}RunInfo_t, *pRunInfo_t;

#define ExportTestInit(fn, level) GNU_ATTRIBUTE(section(".test.init." #level), unused) static InitInfo_t fn##_init_info = {fn, __TC_FN__ ":" #fn};

#define ExportTestCase(fn, level) GNU_ATTRIBUTE(section(".test.run." #level), unused)  static RunInfo_t fn##_run_info = {fn, __TC_FN__ ":" #fn};
