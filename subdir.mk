OBJS = $(OBJ) main.o
TEST_OBJ += $(OBJ)

HEADERS += -I"."

-include RB-Tree/base/subdir.mk
-include RB-Tree/component/subdir.mk
-include RB-Tree/implement/subdir.mk
-include MemMana/subdir.mk

-include RB-Tree/testCases/subdir.mk
-include TestFrame/subdir.mk
